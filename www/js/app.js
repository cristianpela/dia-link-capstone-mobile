// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

/// <reference path="../../typings/main.d.ts" />

angular.module('starter', ['ionic', 'chart.js', 'starter.services', 'starter.factories', 'starter.controllers', 'starter.filters', 'starter.directives'])

  //"browser http://localhost:3000/api emulator http://10.0.2.2:3000/api")
  .constant("BASE_URL", (function () {
    var isBrowser = (window.cordova) ? false : true;
    var domain = isBrowser ? "localhost" : "10.0.2.2";
    var port = ":3000";
    return "http://" + domain + port + "/api";
  })())
  .constant("APP_TYPE", {
    MOBILE :1,
    WEB: 0,
    selected: 1
  })
  .run(function ($ionicPlatform, PopUpFactory, $rootScope) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
      $rootScope.$on('web.error', function (e, arg) {
        PopUpFactory.alert(arg.title, arg.template);
      })
    });
  })
  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    // error interceptor
    $httpProvider.interceptors.push('ErrorInterceptorFactory');
    
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      })
      .state('register', {
        url: '/register',
        templateUrl: 'templates/register.html',
        controller: 'RegisterCtrl'
      })
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })
      .state('app.search', {
        url: '/search/:like',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/search.html',
            controller: 'SearchCtrl'
          }
        }
      })
      .state('app.messages', {
        url: '/messages',
        views: {
          'menuContent': {
            templateUrl: 'templates/messages.html',
            controller: 'MessagesCtrl'
          }
        }
      })
      .state('app.events', {
        url: '/events',
        views: {
          'menuContent': {
            templateUrl: 'templates/events.html',
            controller: 'EventsCtrl'
          }
        }
      })
      .state('app.meetings', {
        url: '/meetings',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/meetings.html',
            controller: 'MeetingsCtrl'
          }
        }
      })
      .state('app.profile', {
        url: '/profile/:id',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/profile.html',
            controller: 'ProfileCtrl'
          }
        },
        resolve: {
          user: function (UserService, $stateParams) {
            return UserService.userDetails($stateParams.id);
          },
          followStatus: function (UserService, $stateParams) {
            return UserService.checkFollowStatus($stateParams.id);
          }
        }
      })
      .state('app.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'templates/home.html',
            controller: 'HomeCtrl'
          }
        }
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');
  });
