/// <reference path="../../typings/main.d.ts" />

angular.module('starter.controllers', [])
  .controller('AppCtrl', function ($scope, $state, PopUpFactory, AuthService, SearchService, CurrentLoggedUser) {
    $scope.user = CurrentLoggedUser.me();
    $scope.what = { like: '' };
    $scope.searchSettingsModal = PopUpFactory.modal('templates/search-settings.html', $scope);

    $scope.logout = function () {
      AuthService.logout().then(function () {
        $state.go('login');
      });
    };
    $scope.search = function () {
      if ($state.current.name === 'app.search') {
        $scope.$broadcast(SearchService.BROADCAST_NEW_SEARCH, { like: $scope.what.like });
      } else {
        $state.go('app.search', { like: $scope.what.like });
      };
    };
    $scope.searchSettings = function () {
      $scope.searchSettingsModal.show();
    };
    $scope.close = function () {
      $scope.searchSettingsModal.hide();
    };
    $scope.goToCheckInReports = function () {
      $state.go('app.profile', { id: $scope.user.id });
    };
    $scope.$on(SearchService.BROADCAST_CRITERIA, function (event, criteria) {
      $scope.criteria = criteria;
    });
    $scope.updateSearchCriteria = function () {
      SearchService.setCriteria($scope.criteria, true);
      $scope.close();
    };
  })
  .controller('HomeCtrl', function ($ionicListDelegate, $scope, $q, PopUpFactory, UserService, SearchService, UtilService, CurrentLoggedUser) {

    var me = CurrentLoggedUser.me();
    $scope.user = me;
    SearchService.setCriteria({ by: 'username', city: me.location.split(',')[0] }, true);

    $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      if (fromState.name === 'app.profile') {
        setButtonTitles();
      }
    });

    var showFollowings = function () {
      $scope.hasRemove = true;
      $scope.hasAccept = false;
      $scope.social = CurrentLoggedUser.followings();
      $scope.listid = 'followings';
    };

    var showPendingFollowers = function () {
      $scope.hasAccept = true;
      $scope.hasRemove = false;
      $scope.social = CurrentLoggedUser.pendingFollowers();
      $scope.listid = 'pendingFollowers';
    }

    var showFollowers = function () {
      $scope.hasRemove = true;
      $scope.hasAccept = false;
      $scope.social = CurrentLoggedUser.followers();
      $scope.listid = 'followers';
    };

    function setButtonTitles() {
      var count = CurrentLoggedUser.allFollowCount();
      var followerTitle = 'Followers (' + count.follower + ')';
      var followingTitle = 'Following (' + count.following + ')';
      var pendingTitle = 'Requests (' + count.pendingFollower + ')';
      if ($scope.buttonNames === undefined) {
        $scope.buttonNames = [followingTitle];
        if (me.isPatient) {
          $scope.buttonNames.push(followerTitle);
          $scope.buttonNames.push(pendingTitle);
        }
      } else {
        $scope.buttonNames[0] = followingTitle;
        if (me.isPatient) {
          $scope.buttonNames[1] = followerTitle;
          $scope.buttonNames[2] = pendingTitle;
        }
      }
    };
    setButtonTitles();

    $scope.hasRemove = true;
    if (!me.isPatient || (me.isPatient && CurrentLoggedUser.followers().length === 0)) {
      $scope.social = CurrentLoggedUser.followings();
      $scope.listid = 'followings';
    } else {
      $scope.social = CurrentLoggedUser.followers();
      $scope.listid = 'followers';
      $scope.selectedButton = 1;
    }

    $scope.buttonClicked = function (index) {
      switch (index) {
        case 0:
          showFollowings();
          break;
        case 1:
          showFollowers();
          break;
        case 2:
          showPendingFollowers();
          break;
      }
    };

    $scope.followAction = function (name, userid, listid) {
      if (name === 'accept') {
        UserService.acceptFollower(userid).then(function () {
          setButtonTitles();
        });
      } else if (name === 'remove') {
        PopUpFactory.confirm('Warning', 'Are sure you want to remove this follow').then(function (res) {
          return (res) ? UserService.removeFollow(listid, userid) : $q.reject();
        }).then(function () {
          setButtonTitles();
        });
      }
      $ionicListDelegate.closeOptionButtons();
    };

    //edit profile
    $scope.editModal = PopUpFactory.modal('templates/edit-profile.html', $scope);
    $scope.edit = {
      about: me.about
    };
    $scope.openEditProfile = function () {
      $scope.editModal.show();
    };
    $scope.updateProfile = function () {
      UserService.updateDetails($scope.edit).then(function () {
        me.about = $scope.edit.about;
      }).finally(function () {
        $scope.close();
      });
    };

    $scope.close = function(){
       $scope.editModal.hide();
    };

  })
  .controller('SearchCtrl', function ($scope, $stateParams, SearchService, $timeout) {
    var SCROLL_EVENT = 'scroll.infiniteScrollComplete';//'scroll.refreshComplete';
    var limit = 10;
    var searchQuery;

    function init(like) {
      $scope.like = like;
      $scope.results = [];
      $scope.criteria = SearchService.criteria();
      $scope.totalItems = -1;
      $scope.moreItems = true;

      searchQuery = SearchService.search($scope.like);
      searchQuery.count().then(function (result) {
        $scope.totalItems = result.data.count;
        console.log("count results: " + $scope.totalItems);
      });
    }
    init($stateParams.like);

    $scope.$on(SearchService.BROADCAST_NEW_SEARCH, function (event, like) {
      console.log("reset search controller state");
      init(like.like); // reset state
      $scope.$digest();// refresh the bind with view
    });

    function search(skip) {
      // if (!$scope.noMoreItems) {
      searchQuery.paging($scope.results.length, limit).then(function (results) {
        if ($scope.totalItems === $scope.results.length) {
          $scope.moreItems = false;
        }
        Array.prototype.push.apply($scope.results, results);
      }).finally(function () {
        $scope.$broadcast(SCROLL_EVENT);
      });
      //     }
    }

    $scope.searchMore = function () {
      search();
    };

    $scope.$on(SCROLL_EVENT, function () {
      console.log('done scrolling');
    });

  })
  .controller('MessagesCtrl', function ($ionicListDelegate, $scope, $stateParams, PopUpFactory, MessageService, CurrentLoggedUser, UserService) {

    $scope.toId = $stateParams.toId;
    $scope.to = $stateParams.to;

    $scope.refresh = function () {
      MessageService.getMessages().then(function (response) {
        $scope.messages = response.data;
      });
    };

    $scope.refresh();

    $scope.messageModal = PopUpFactory.modal('templates/message-entry.html', $scope);
    $scope.newMessageModal = PopUpFactory.modal('templates/new-message-entry.html', $scope);

    $scope.close = function () {
      $scope.newMessageModal.hide();
      $scope.messageModal.hide();
    };

    $scope.openReply = function (index) {
      index = (index == undefined) ? $scope.openedMessage.indexRef : index;
      var parentMessage = $scope.messages[index];
      $scope.newMessage = {};
      $scope.newMessage.title = '[RE]' + parentMessage.title;
      $scope.newMessage.body = '[' + parentMessage.body + ']';
      $scope.newMessage.from = CurrentLoggedUser.me().fullname + ' (' + CurrentLoggedUser.me().username + ')';
      $scope.to = { auto: true };
      $scope.to.name = parentMessage.from;
      $scope.to.id = parentMessage.fromId;
      $scope.newMessageModal.show();
      $ionicListDelegate.closeOptionButtons();
    };

    $scope.openNewMessage = function () {
      $scope.newMessage = {};
      $scope.to = { auto: false };
      $scope.newMessage.from = CurrentLoggedUser.me().fullname + ' (' + CurrentLoggedUser.me().username + ')';
      $scope.newMessageModal.show();
      $ionicListDelegate.closeOptionButtons();
    };

    $scope.openMessage = function (index) {
      $scope.openedMessage = $scope.messages[index];
      $scope.openedMessage.indexRef = index;
      $scope.messageModal.show();
      MessageService.markAsRead($scope.openedMessage.id).then(function () {
        $scope.messages[index].viewed = true;
      });
      $ionicListDelegate.closeOptionButtons();
    };

    $scope.deleteMessage = function (index) {
      index = (index == undefined) ? $scope.openedMessage.indexRef : index;
      MessageService.deleteMessage($scope.messages[index].id).then(function () {
        $scope.messages.splice(index, 1);
        return PopUpFactory.alert('Notification', 'Message was deleted');
      }).then(function () {
        $scope.close();
        $ionicListDelegate.closeOptionButtons();
      });
    };

    $scope.sendMessage = function () {
      var sendPromise;
      if ($scope.to.id) {
        sendPromise = MessageService.postMessage($scope.to.id, $scope.newMessage);
      } else {
        sendPromise = UserService.findUserIdByUsername($scope.to.name).then(function (toId) {
          return MessageService.postMessage(toId, $scope.newMessage);
        });
      }
      sendPromise.then(function () {
        PopUpFactory.alert('Notification', 'Message has been sent');
      }).then(function () {
        $scope.close();
      }).catch(function (err) {
        PopUpFactory.alert('Username Error', err);
      });
    };
  })
  .controller('EventsCtrl', function ($scope, $stateParams) {

  })
  .controller('MeetingsCtrl', function ($scope, $stateParams, PopUpFactory, MeetingService, CurrentLoggedUser) {

    $scope.isPatient = CurrentLoggedUser.me().isPatient;
    $scope.meetings = [];

    if ($scope.isPatient) {
      $scope.newMeeting = {};
      $scope.newMeetingModal = PopUpFactory.modal('templates/new-meeting.html', $scope);
      $scope.close = function () {
        $scope.newMeetingModal.hide();
      }
      $scope.openCreate = function () {
        $scope.newMeetingModal.show();
      };
    }

    MeetingService.getMeetings().then(function (meetings) {
      $scope.meetings = meetings;
    });
    $scope.showAttenders = function (meetId) {
      MeetingService.getAttenders(meetId).then(function (attenders) {
        $scope.attenders = attenders;
        PopUpFactory.show('templates/show-attenders.html', 'Attenders:', $scope);
      });
    };
    $scope.join = function (index) {
      MeetingService.join($scope.meetings[index].id).then(function (response) {
        $scope.meetings[index].attendid = response.data.id;
        $scope.meetings[index].joined = true;
      });
    };
    $scope.leave = function (index) {
      var meetid = $scope.meetings[index].id;
      var attendid = $scope.meetings[index].attendid;
      MeetingService.leave(meetid, attendid).then(function () {
        $scope.meetings[index].joined = false;
      });
    };
    $scope.cancel = function (index) {
      MeetingService.cancel($scope.meetings[index].id).then(function (response) {
        $scope.meetings.splice(index, 1);
      });
    };
    $scope.create = function () {
      $scope.newMeeting.date.setHours(($scope.newMeeting.time.getHours()));
      $scope.newMeeting.date.setMinutes(($scope.newMeeting.time.getMinutes()));
      $scope.newMeeting.time = undefined;
      MeetingService.create($scope.newMeeting).then(function (response) {
        var newMeeting = $scope.newMeeting;
        newMeeting.owner = true;
        newMeeting.id = response.data.meetingId;
        newMeeting.username = CurrentLoggedUser.me().username;
        newMeeting.fullname = CurrentLoggedUser.me().fullname;
        $scope.meetings.push(newMeeting);
        $scope.newMeeting = {}; //reset
        $scope.close();
      });
    };
  })
  .controller('ProfileCtrl', function ($ionicListDelegate, $scope, $state, $stateParams, $q, PopUpFactory, user, followStatus, UserService, CheckInService, MessageService, CurrentLoggedUser) {

    $scope.user = user;

    var status = followStatus.data.status;

    function loadReports(timeFilter) {
      CheckInService.getReports(user.id, timeFilter).then(function (response, timeFilter) {
        $scope.reports = response.data;
      });
    }

    function loadCheckInRequestsCount() {
      CheckInService.countRequests().then(function (result) {
        $scope.countReq = result.data.count;
      });
    }

    $scope.newMessageModal = PopUpFactory.modal('templates/new-message-entry.html', $scope);
    $scope.reportModal = PopUpFactory.modal('templates/checkin-report.html', $scope);
    $scope.newReportModal = PopUpFactory.modal('templates/new-checkin-report.html', $scope);
    $scope.giChartModal = PopUpFactory.modal('templates/gi-chart.html', $scope);

    $scope.showStatuses = [];

    if (status.length === 0) {
      if (user.isPatient) {
        $scope.showAdd = true;
      }
      addStatusToView("No Status With You");
    } else if (status[0].type === 'owner') {
      $scope.isOwner = true;
      if (user.isPatient) {
        $scope.showReports = true;
        loadCheckInRequestsCount();
        loadReports('month');
      }// own reports
    } else {
      // relation of follower or followee, from current logged in user's perspective
      // Ex if this profile page is a followee then this means that current loogged is following them
      var statusFollowee = (status[0].type === 'followee') ? status[0] : (status[1]) ? status[1] : undefined;
      var statusFollower = (status[0].type === 'follower') ? status[0] : (status[1]) ? status[1] : undefined;

      if (statusFollowee) {
        if (statusFollowee.pending) {
          addStatusToView("Pending");
        } else {
          addStatusToView("Following");
          $scope.showRemoveFollowee = true;
          $scope.showReports = true;
          loadReports('month');
        }
      }
      if (statusFollower) {
        if (statusFollower.pending) {
          addStatusToView("Pending");
          $scope.showAccept = true;
        } else {
          addStatusToView("Follower");
          $scope.showRemoveFollower = true;
        }
      }
      if (user.isPatient && !statusFollowee) {
        $scope.showAdd = true;
      }
    }

    $scope.add = function () {
      UserService.addAsFollower(user.id).then(function () {
        $scope.showAdd = false;
        addStatusToView("Pending");
      });
    };

    $scope.close = function () {
      $scope.reportModal.hide();
      $scope.newReportModal.hide();
      $scope.giChartModal.hide();
      $scope.newMessageModal.hide();
    };

    $scope.openNewMessage = function () {
      var me = CurrentLoggedUser.me();
      $scope.newMessage = {};
      $scope.to = { auto: true };// username is preset
      $scope.newMessage.from = me.fullname + ' (' + me.username + ')';
      $scope.to.name = user.username;
      $scope.newMessageModal.show();
    };

    $scope.sendMessage = function () {
      MessageService.postMessage(user.id, $scope.newMessage).then(function () {
        return PopUpFactory.alert('Notification', 'Message has been sent!');
      }).then(function () {
        $scope.close();
      });
    }

    $scope.showReportFilterSheet = function () {
      PopUpFactory.choice('<center>Report Filter</center>',
        ['This Month', 'This Year', 'All Time'],
        function (index) {
          switch (index) {
            case 0:
              loadReports('month');
              break;
            case 1:
              loadReports('year');
              break;
            default:
              loadReports();
              break;
          }
        });
    };

    function removeFollowee() {
      UserService.removeFollow("followings", user.id).then(function () {
        $scope.showReports = false;
        $scope.showAdd = true;
        $scope.showRemoveFollowee = false;
        removeStatusFromView('Following');
      });
    }

    function removeFollower() {
      UserService.removeFollow("followers", user.id).then(function () {
        $scope.showRemoveFollower = false;
        removeStatusFromView('Follower');
      });
    }

    function addStatusToView(statusTitle) {
      var noStatusIndex = $scope.showStatuses.indexOf("No Status With You");
      if (noStatusIndex !== -1 && $scope.showStatuses.length > 0) {
        $scope.showStatuses.splice(noStatusIndex, 1);
      }
      $scope.showStatuses.push(statusTitle);
    }

    function removeStatusFromView(statusTitle) {
      $scope.showStatuses.splice($scope.showStatuses.indexOf(statusTitle), 1)
      if ($scope.showStatuses.length === 0) {
        $scope.showStatuses.push("No Status With You");
      }
    }

    $scope.accept = function () {
      UserService.acceptFollower(user.id).then(function () {
        $scope.showRemoveFollower = true;
        $scope.showAccept = false;
        removeStatusFromView('Pending');
        addStatusToView('Follower');
      });
    };

    $scope.removeFollow = function () {
      if ($scope.showRemoveFollowee && $scope.showRemoveFollower) {
        PopUpFactory.choice('<center>Follow Removal</center>',
          ['Remove Follower', 'Stop Following', 'Cancel'],
          function (index) {
            if (index === 0) {
              removeFollower();
            } else {
              removeFollowee();
            }
          });
      } else {

        var template = ($scope.showRemoveFollowee) ? 'Are you sure you want to stop following?' :
          'Are you sure you want to remove this follower?';
        var followType = ($scope.showRemoveFollowee) ? 'followings' : 'followers';

        PopUpFactory.confirm('Warning', template).then(function (res) {
          return (res) ? UserService.removeFollow(followType, user.id) : $q.reject();
        }).then(function () {
          if ($scope.showRemoveFollowee) {
            $scope.showReports = false;
            $scope.showRemoveFollowee = false;
            removeStatusFromView('Following');
          } else {
            $scope.showRemoveFollower = false;
            removeStatusFromView('Follower');
          }
          $scope.showAdd = true;
        });
      }
    };

    $scope.checkIn = function () {
      if ($scope.isOwner) {
        $scope.newReport = { answers: [] };
        CheckInService.getQuestions().forEach(function (q) {
          var entry = {
            question: q.body,
            answer: ''
          }
          $scope.newReport.answers.push(entry);
        });
        $scope.newReportModal.show();
      } else {
        CheckInService.sendRequest(user.id).then(function () {
          PopUpFactory.alert('Notification', 'Check In Request was sent to your follower!');
        });
      }
    };

    $scope.showReportDetail = function(reportId, date){
         var pr = CheckInService.getReportDetail(user.id, reportId).then(function (response) {
            $scope.currentReportDate  = date;
            $scope.reportAnswers = response.data;
            $scope.reportModal.show();
        });
    };

    $scope.submitReport = function () {
      CheckInService.postReport($scope.newReport).then(function () {
        $scope.close();
        loadReports('month');
      });
    };

    $scope.giChart = function () {
      CheckInService.getGlycemicChartData(user.id).then(function (chart) {
        $scope.labels = chart.labels;
        $scope.data = [chart.data];
        $scope.series = ['Blood Sugar Values'];
        $scope.giChartModal.show();
      });
    };


    $scope.showCheckInRequests = function () {
      CheckInService.getRequests()
        .then(function (response) {
          $scope.checkInReqs = response.data;
          return PopUpFactory.show('templates/show-checkin-requests.html', 'Check In Requests From:', $scope);
        })
        .then(function () {
          return CheckInService.acknowledgeRequests();
        })
        .then(function () {
          $scope.countReq = 0;
        });
    };
  })
  .controller('LoginCtrl', function ($ionicHistory, $scope, $state, PopUpFactory, AuthService, UserService) {
    $scope.$on('$ionicView.enter', function (e) {
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
    });

    $scope.data = {
    };

    var modal = PopUpFactory.modal('templates/register.html', $scope);

    $scope.signup = function () {
      AuthService.register($scope.data).then(function () {
        modal.hide();
      });
    }

    $scope.goToRegister = function () {
      modal.show();
    };

    $scope.close = function () {
      modal.hide();
    };

    $scope.loginData = {
    };

    $scope.login = function () {
      AuthService.login($scope.loginData)
        .then(function (user) {
          $state.go('app.home');
        });
    };
  });
