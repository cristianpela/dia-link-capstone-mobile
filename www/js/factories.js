angular.module('starter.factories', [])
    .factory('ErrorInterceptorFactory', function ($q, $rootScope) {
        return {
            'responseError': function (errorResponse) {
                var title, template;
                if (errorResponse.status === -1) {
                    title = "Connection Refused";
                    template = "Could not connect to server";
                } else {
                    title = errorResponse.data.error.name + '(' + errorResponse.status + ')';
                    template = errorResponse.data.error.message;
                }
                $rootScope.$broadcast('web.error', {
                    title: title,
                    template: template
                });
                return $q.reject(errorResponse);
            }
        };
    }).factory('AvatarPathFactory', function (BASE_URL) {
        return {
            getPath: function (id) {
                return BASE_URL + '/ProfileContainers/' + id + '/download/avatar.jpg';
            }
        }
    }).factory('CurrentLoggedUser', function () {

        var _pendingFollowers = [];
        var _followers = [];
        var _followings = [];

        var _allFollowCount = {};
        var _me = {};

        return {
            me: function (newMe, updateMe) {
                if (newMe) {
                    if (updateMe) {
                        for (key in newMe) {
                            _me[key] = newMe[key];
                        }
                    } else {
                        _me = newMe;
                    }
                }
                return _me;
            },
            pendingFollowers: function (newData) {
                if (newData) {
                    Array.prototype.push.apply(_pendingFollowers, newData);
                }
                return _pendingFollowers;
            },
            followers: function (newData) {
                if (newData) {
                    Array.prototype.push.apply(_followers, newData);
                }
                return _followers;
            },
            followings: function (newData) {
                if (newData) {
                    Array.prototype.push.apply(_followings, newData);
                }
                return _followings;
            },
            allFollowCount: function (newData, op) {
                if (newData) {
                    _allFollowCount = newData;
                }
                return _allFollowCount.count;
            },
            allCrement: function (prop, unit) {
                unit = unit || 1;
                _allFollowCount.count[prop] = _allFollowCount.count[prop] + unit;
            },
            clear: function () {
                _pendingFollowers = [];
                _followers = [];
                _followings = [];
                _allFollowCount = {};
                _me = {};
            }
        }
    }).factory('PopUpFactory', function (APP_TYPE, $injector, $window) {
        var interface;
        if (APP_TYPE.selected === APP_TYPE.MOBILE) {
            interface = $injector.get('MobilePopUpFactory').interface;
        } else {
            interface = $injector.get('BrowserPopUpFactory').interface;
        }
        return {
            alert: interface.alert,
            confirm: interface.confirm,
            modal: interface.modal,
            show: interface.show,
            choice: interface.choice
        };
    }).factory('MobilePopUpFactory', function ($ionicPopup, $ionicModal, $ionicActionSheet) {
        var interface = {
            alert: angular.noop,
            modal: angular.noop,
            confirm: angular.noop,
            show: angular.noop,
            choice: angular.noop
        };
        interface.alert = function (title, template) {
            return $ionicPopup.alert({
                title: title,
                template: template
            });
        };
        interface.confirm = function (title, template) {
            return $ionicPopup.confirm({
                title: title,
                template: template
            });
        };
        interface.modal = function (templateUrl, scope) {
            var _modal = {};
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: scope
            }).then(function (modal) {
                _modal = modal;
            });
            return {
                show: function () { _modal.show(); },
                hide: function () { _modal.hide(); }
            }
        };
        interface.show = function (templateUrl, title, $scope) {
            return $ionicPopup.show({
                templateUrl: templateUrl,
                title: title,
                scope: $scope,
                buttons: [
                    { text: 'Close' }
                ]
            });
        };
        interface.choice = function (title, buttonsTextArr, onSelected) {
            var buttons = buttonsTextArr.map(function (text) {
                return { text: text };
            });
            var filterSheet = $ionicActionSheet.show({
                buttons: buttons,
                titleText: title,
                buttonClicked: function (index) {
                    onSelected(index);
                    filterSheet();
                    return true;
                }
            });
        };
        return {
            interface: interface
        };
    });